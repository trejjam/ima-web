//http://marco-difeo.de/2011/06/13/jquery-rectangle-selection/

/// <reference path="typings/tsd.d.ts" />
/// <reference path="commonFunc.ts" />

var debugSelect = true;

module Select {
	interface ICoordinates {
		left: number;
		top: number;
		width: number;
		height: number;
		x1:number;
		x2:number;
		y1:number;
		y2:number;
	}
	interface IFormCoordinates {
		x1: string;
		x2: string;
		y1: string;
		y2: string;
	}
	class Coordinates {
		x1:number;
		x2:number;
		y1:number;
		y2:number;

		constructor(public top:number, public left:number, public width:number, public height:number) {
			this.x1 = left;
			this.x2 = left + width;
			this.y1 = top;
			this.y2 = top + height;
		}
	}
	export class FormCoordinates {
		constructor(public x1:string, public x2:string, public y1:string, public y2:string) {
		}
	}

	export class Rect {
		private static gMOUSEUP:boolean = false;
		private static gMOUSEDOWN:boolean = false;
		private static gEventInit:boolean = false;
		private selected:boolean;
		private x1:number;
		private x2:number;
		private y1:number;
		private y2:number;

		constructor(public form:JQuery, public  image:JQuery, public  drawPlace:JQuery, public  selection:JQuery, public formInputs, public mouseUpHide:boolean = true) {
			Rect.globalEvent();
			console.log(this);
			this.imageInit();

			if (debugSelect) console.log("Select.Rec initted");
		}

		private static globalEvent() {
			if (Rect.gEventInit) {
				if (debugSelect) console.log("global event inited yet");
				return;
			}

			// Global Events if left mousebutton is pressed or nor (usability fix)
			$(document).mouseup(function () {
				Rect.gMOUSEUP = true;
				Rect.gMOUSEDOWN = false;
			}).mousedown(function () {
				Rect.gMOUSEUP = false;
				Rect.gMOUSEDOWN = true;
			});

			Rect.gEventInit = true;
		}

		private imageInit() {
			this.drawPlace.mousedown(function (e) {
				this.selected = true;
				// store mouseX and mouseY
				this.x1 = e.pageX;
				this.y1 = e.pageY;
			}.bind(this));
			// Selection complete, hide the selection div (or fade it out)
			this.drawPlace.mouseup(function () {
				this.selected = false;
				var coordinate:ICoordinates = this.getObject();
				this.save(coordinate);

				if (this.mouseUpHide) this.selection.hide();
			}.bind(this));
			// Usability fix. If mouse leaves the selection and enters the selection frame again with mousedown
			this.drawPlace.mouseenter(function () {
				(Rect.gMOUSEDOWN) ? this.selected = true : this.selected = false;
			}.bind(this));
			// Usability fix. If mouse leaves the selection and enters the selection div again with mousedown
			this.selection.mouseenter(function () {
				(Rect.gMOUSEDOWN) ? this.selected = true : this.selected = false;
			}.bind(this));
			// Set selection to false, to prevent further selection outside of your selection frame
			this.drawPlace.mouseleave(function () {
				this.selected = false;
			}.bind(this));

			// If selection is true (mousedown on selection frame) the mousemove
			// event will draw the selection div
			this.drawPlace.mousemove(function (e) {
				if (this.selected) {
					// Store current mouseposition
					this.x2 = e.pageX;
					this.y2 = e.pageY;

					// Prevent the selection div to get outside of your frame
					//(x2+this.offsetleft < 0) ? selection = false : ($(this).width()+this.offsetleft < x2) ? selection = false : (y2 < 0) ? selection = false : ($(this).height() < y2) ? selection = false : selection = true;;
					// If the mouse is inside your frame resize the selection div
					if (this.selected) {
						// Calculate the div selection rectancle for positive and negative values

						var coordinate:ICoordinates = this.getObject();
						// Use CSS to place your selection div
						this.selection.css({
							position: 'absolute',
							zIndex: 5000,
							left: coordinate.left,
							top: coordinate.top,
							width: coordinate.width,
							height: coordinate.height
						});
						this.selection.show();
					}
				}
			}.bind(this));
		}

		private getObject():ICoordinates {
			var imagePosition:JQueryCoordinates = this.image.offset();

			return new Coordinates(
				((this.y1 < this.y2) ? this.y1 : this.y2) - imagePosition.top,
				((this.x1 < this.x2) ? this.x1 : this.x2) - imagePosition.left,
				(this.x1 < this.x2) ? this.x2 - this.x1 : this.x1 - this.x2,
				(this.y1 < this.y2) ? this.y2 - this.y1 : this.y1 - this.y2
			);
		}

		private save(coordinate:ICoordinates) {
			this.form.find("input[name='" + this.formInputs.x1 + "']").val(coordinate.x1.toString());
			this.form.find("input[name='" + this.formInputs.x2 + "']").val(coordinate.x2.toString());
			this.form.find("input[name='" + this.formInputs.y1 + "']").val(coordinate.y1.toString());
			this.form.find("input[name='" + this.formInputs.y2 + "']").val(coordinate.y2.toString());

			// Info output
			if (debugSelect) console.log('( x1 : ' + coordinate.x1 + ' )  ( x2 : ' + coordinate.x2 + ' )  ( y1 : ' + coordinate.y1 + '  )  ( y2 : ' + coordinate.y2 + ' )');
		}
	}
}