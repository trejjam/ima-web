//http://marco-difeo.de/2011/06/13/jquery-rectangle-selection/

/// <reference path="typings/tsd.d.ts" />
/// <reference path="commonFunc.ts" />

var debugIMAMap = true;

module IMAMap {
	export class Filter {
		select:JQuery;

		constructor(public filter:JQuery, public map:JQuery, public selectName:string = "room") {
			this.select = this.filter.find("select[name='" + selectName + "']");
			this.select.change(this.change.bind(this));

			if (debugIMAMap) console.log("IMAMap.Filter initted");
		}

		private change() {
			if (this.select.val() == 0) {
				this.map.children().show();
			}
			else {
				this.map.children().hide();
				this.map.children(".room_" + this.select.val()).show();
			}
		}
	}
}