/**
 * Created by Jan on 6. 10. 2014.
 */

/// <reference path="typings/tsd.d.ts" />
/// <reference path="commonFunc.ts" />

var debugPopups = true;
if (typeof popupsStack == "undefined") var popupsStack:Array<Popups.Values> = Array();

module Popups {
	var root:JQuery;
	var id:number = -1;
	var popups:Array<Popup> = [];
	var initted:boolean = false;

	export class Values {
		nadpis:string = "";
		text:string = "";
		width:number = 200;
		actionAfter:{(): void;} = null;
	}
	export class Popup {
		id:number = null;
		afterHide:{(): void;} = null;
		escListen:boolean = false;
		escEvent:JQuery = null;

		constructor(id:number, def:Values, createNew:boolean = true) {
			this.id = id;

			if (isset(def) && def != null) {
				var emptyValues:Values = new Values();

				if (!("nadpis" in def)) def.nadpis = emptyValues.nadpis;
				if (!("text" in def)) def.text = emptyValues.text;
				if (!("width" in def)) def.width = emptyValues.width;
				if (!("actionAfter" in def)) def.actionAfter = emptyValues.actionAfter;

				this.setActionAfterHide(def.actionAfter);

				if (createNew) {
					root.append('<div class="popup" id="popup' + this.id + '" data-popup-id="' + this.id + '"><div class="bg"></div><div class="innerPopup" style="width: ' + def.width + 'px"><a href="" onclick="return Popups.get(' + this.id + ').hide();" class="close"></a><div class="nadpis">' + def.nadpis + '</div><div class="text">' + def.text + '</div></div></div>');
					root.find("#popup" + this.id).find(".bg").click(function () {
						if (debugPopups) console.log(this);
						var id:number = Number($(this).parent().attr("data-popup-id"));
						Popups.get(id).hide();
					});
				}
			}
		}

		edit(val:Values) {
			var el = root.find("#popup" + this.id).find(".innerPopup");
			var emptyValues:Values = new Values();

			if (val.width != emptyValues.width) el.css("width", val.width + "px");
			if (val.nadpis != emptyValues.nadpis) el.find(".nadpis").html(val.nadpis);
			if (val.text != emptyValues.text) el.find(".text").html(val.text);
			if (val.actionAfter != emptyValues.actionAfter) this.afterHide = val.actionAfter;

			return this;
		}

		show(h?:number) {
			var t:number = $(window).scrollTop();
			var h:number = isset(h) ? (t < 60 ? (60 + h) : h) : ($(window).height());

			$("#popup" + this.id).css("margin-top", t + h / 3 + "px").show();

			if (debugPopups) console.log(this.id);

			this.escListener();
			return this;
		}

		hide() {
			$("#popup" + this.id).hide();

			this.escListenerDeactivate();
			this.actionAfterHide();

			return this;
		}

		private actionAfterHide() {
			var action = this.afterHide;
			if (typeof action == "function") {
				action();
			}
		}

		setActionAfterHide(func) {
			this.afterHide = func;

			return this;
		}

		setEscListener(state:boolean = true) {
			this.escListen = state;

			return this;
		}

		private escListener() {
			if (this.escListen) {
				this.escEvent = $(window).keydown(function (popup) {
					return function (e:KeyboardEvent) {
						if (e.which == 27) {
							console.log(e);
							e.preventDefault();

							popup.hide();
						}
					};
				}(this));
			}
		}

		private escListenerDeactivate() {
			if (this.escListen) {
				this.escEvent.off();
			}
		}
	}

	export function setNew(def:any);
	export function setNew(def:Values) {
		id++;

		popups[id] = new Popup(id, def);

		return popups[id];
	}

	export function importStack() {
		if ((typeof popupsStack == "object" && popupsStack.constructor == Array)) {
			while (popupsStack.length) {
				var def:Values = popupsStack.pop();
				setNew(def).show();
			}
		}

		//popupsStack = Array();
	}

	export function get(id:number):Popup {
		return popups[id];
	}

	function init() {
		root = $("#popups");
		var bgs = root.find(".popup").find(".bg");
		var i;

		for (i = 0; i < bgs.length; i++) {
			var bg = bgs[i];

			var popupRoot = $(bg).parent();

			id++;
			popups[id] = new Popup(id, null, false);
			popupRoot.attr("data-popup-id", id).attr("id", "popup" + id);

			var closeFunc = function (e:MouseEvent) {
				e.preventDefault();

				if (debugPopups) console.log(Number($(this).closest(".popup").attr("data-popup-id")));
				Popups.get(Number($(this).closest(".popup").attr("data-popup-id"))).hide();

				return false;
			};

			$(bg).click(closeFunc);
			popupRoot.find(".close").click(closeFunc);

			var defVal = popupRoot.attr("data-popup-default-val");
			if (isset(defVal)) {
				var defValObj = JSON.parse(defVal);

				if (debugPopups) console.log(defValObj);
				Popups.get(id).edit(defValObj);
			}

			if (popupRoot.hasClass("show")) {
				Popups.get(id).show();
			}
		}

		importStack();

		if (debugPopups) console.log("popups initted");
		initted = true;
	}

	export function getRoot():JQuery {
		return root;
	}

	init();
}
