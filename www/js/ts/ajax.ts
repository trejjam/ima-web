/**
 * Created by Jan on 7. 10. 2014.
 */

/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="commonFunc.ts" />

module Ajax {
	class post {
		constructor(url:string, data, callback:{(send): void;}, failcallback:{(send): void;}) {
			var send = $.ajax({
				type: "POST",
				url: url,
				data: data,
				dataType: "json"
			}).done(function () {
				if (typeof callback == "function") callback(send);
				if (debug) console.log(this);
			}).fail(function () {
				if (typeof failcallback == "function") failcallback(send);
			});
		}
	}
	;
}
