/**
 * Created by Jan on 7. 10. 2014.
 */

/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="nette.ajax.ts" />

var debug = true;

if (typeof console != "object") {
	this.console = {
		log: function (msg) {
		}
	}
}

function isset(el) {
	return (typeof el != "undefined");
}
function imgLoaded(imgEl) {
	var img = new Image();

	$(imgEl).attr("data-img-loaded", "false");
	img.onload = function () {
		if (debug) console.log("imgLoaded", imgEl);
		$(imgEl).attr("data-img-loaded", "true");
	}
	img.src = $(imgEl).attr("src");
}
function loadImg(path, afterload) {
	var img = new Image();

	img.onload = function () {
		if (debug) console.log("imgLoaded", path);
		afterload(path);
	}
	img.src = path;
}
function scrollToEl(el) {
	el = $(el);

	if (typeof el[0] == "undefined") {
		return true;
	}
	var h = el.offset().top;
	$("html, body").animate({
		scrollTop: h
	}, 100);

	return false;
}

$(function () {
	$.nette.init();
});
