$(function () {
	var form = $("[id^='frm-selectRoom-']");

	if (isset(form[0])) {
		var formCoordinates = new Select.FormCoordinates("x1", "x2", "y1", "y2");
		new Select.Rect(form, form.parent().find(".rectSelect img"), form.parent().find(".rectSelect .drawPlace"), form.parent().find(".rectSelect .drawPlace .selection"), formCoordinates, false);
	}

	var filter = $(".showRoom");
	if (isset(filter[0])) {
		new IMAMap.Filter(filter, $(".rectSelect .drawPlace"));
	}
});
