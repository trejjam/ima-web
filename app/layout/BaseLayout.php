<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 15. 11. 2014
 * Time: 19:33
 */

namespace App\Layout;

use App\Model\LabelsService;
use Nette;

class BaseLayout
{
	/**
	 * @var bool
	 */
	protected $debug;
	/**
	 * @var Nette\Security\User
	 */
	private $user;
	/**
	 * @var \App\Model\LabelsService
	 */
	private $labels;
	/**
	 * @var Nette\Caching\Cache
	 */
	private $cache;

	/**
	 * @var array
	 */
	private $myParams = [];

	function __construct($debug, Nette\Caching\Cache $cache, Nette\Security\User $user, \App\Model\LabelsService $labels, \App\Model\PageInfo $pageInfo) {
		$this->debug = $debug;
		$this->cache = $cache;
		$this->user = $user;
		$this->labels = $labels;
		$this->pageInfo = $pageInfo;
	}

	function setParams(array $params) {
		$this->myParams = $params;
	}
	function getParams() {
		return $this->myParams;
	}

	function setTemplate(Nette\Application\UI\ITemplate $template, $pageInfo) {
		$template->fv = $this->myParams["file_version"];
		$template->debug = $this->debug;

		$template->server = $_SERVER['SERVER_NAME'];
		$template->uri = explode("?", isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : "")[0];

		$this->setBrowserHead($template);
		$template->pageInfo = $pageInfo;

		$template->menuItems = $this->getMenu();

		$template->userLogged = ($this->user->loggedIn) ? TRUE : FALSE;

		$template->jsName = "default";
	}

	private function setBrowserHead(Nette\Application\UI\ITemplate $template) {
		$browser = new \Browser();
		$template->browser = Nette\Utils\Strings::webalize($browser->getBrowser());
		$template->IEversion = ($template->browser == "internet-explorer") ? Nette\Utils\Strings::webalize($browser->getVersion()) : "-1";
	}
	private function getMenu() {
		return [
			"Úvod" => ["Hp:", ""],
		];
	}
}

trait BaseLayoutTrait
{
	/**
	 * @var \App\Layout\BaseLayout @inject
	 */
	public $layout;

	/**
	 * @var \App\Model\PageInfo @inject
	 */
	public $pageInfo;
	/**
	 * @var \App\Model\LabelsService @inject
	 */
	public $labels;

	private $drobky = [];

	/**
	 * @var array
	 */
	protected $myParams;

	function beforeRender() {
		$selectedPage = $this->pageInfo->selectPage($this->request);
		$this->layout->setTemplate($this->template, $this->pageInfo->getHead($selectedPage));


		$this->myParams = $this->layout->getParams();

		if (method_exists($this, 'ownBeforeRender')) {
			$this->ownBeforeRender();
		}
	}

	function addDrobek($text, $url) {
		$this->drobky[] = (object)array("text" => $text, "url" => $url);
	}
	function afterRender() {
		parent::afterRender();

		$this->editFlashs();
		$this->template->drobky = $this->drobky;
	}

	function editFlashs() {
		foreach ($this->template->flashes as $k => $v) {
			$this->template->flashes[$k]->text = $this->template->flashes[$k]->message;
			unset($this->template->flashes[$k]->message);

			if (\App\Model\Utils::isJson($v->type)) {
				$flashTypeJson = json_decode($v->type);

				unset($this->template->flashes[$k]->type);
				foreach ($flashTypeJson as $k2 => $v2) {
					$this->template->flashes[$k]->$k2 = $v2;
				}
			}
		}
	}
	public function getJsCache($name = "default") {
		return isset($this->cache) ? (isset($this->cache["jsCache/" . $name]) ? $this->cache["jsCache/" . $name] . ".js" : "") : "";
	}
	public function getJsFiles($listFile = "default") {
		$out = [];

		$dir = rootDir . "/www/js/";

		if (!is_file($dir . $listFile . ".js-list")) {
			return FALSE;
		}

		$jsList = file('safe://' . $dir . $listFile . ".js-list", FILE_IGNORE_NEW_LINES);
		foreach ($jsList as $v) {
			$out[] = $v;
		}

		return $out;
	}
	function createComponentLabels() {
		return $this->labels->create();
	}
}
