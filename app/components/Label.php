<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 4.5.14
 * Time: 23:53
 */

namespace App\Components;

class Label extends \Nette\Application\UI\Control
{
	/**
	 * @var \App\Model\LabelsService
	 */
	private $labelService;

	public function setService(\App\Model\LabelsService $labelsService) {
		$this->labelService = $labelsService;
	}

	public function render($key) {
		echo $this->labelService->$key;
	}
}
