<?php
/**
 * Created by PhpStorm.
 * User: jam
 * Date: 17.12.14
 * Time: 22:13
 */

define('startTime', microtime(TRUE));

umask(7);

if (php_sapi_name() == "cli") {
	if (isset($_SERVER["KEY"]) && $_SERVER["KEY"] == "VPS") {
		$_SERVER["VPS"] = TRUE;
	}
}
if (isset($_SERVER["REQUEST_URI"]) && $_SERVER["REQUEST_URI"] == "/index.php") {
	header("Location: http://ima.tremi.cz/");
	die();
}

define("rootDir", __DIR__ . "/..");

if (php_sapi_name() != 'cli' && file_exists(rootDir . '/app/.maintenance.php') && (!isset($_SERVER["REMOTE_ADDR"]) || $_SERVER["REMOTE_ADDR"] != "78.110.220.34")) {
	require rootDir . '/app/.maintenance.php';
}

require rootDir . "/vendor/autoload.php";

$configurator = new Nette\Configurator;

$configurator->setDebugMode(["127.0.0.1", "127.0.0.10", "127.0.0.11", "78.110.220.34", "90.182.200.84"]);
if (php_sapi_name() == "cli") $configurator->setDebugMode(TRUE);

// Enable Nette Debugger for error visualisation & logging
$configurator->enableDebugger(rootDir . '/log', "jan.trejbal@gmail.com");

// Configure libraries
$configurator->setTempDirectory(rootDir . '/temp');

define("APP_DIR", rootDir . "/app");

$configurator->createRobotLoader()
			 ->addDirectory(APP_DIR)
			 ->register();

define("debug_mode", $configurator->isDebugMode());

// Create Dependency Injection container from config.neon file
$configurator->addConfig(rootDir . '/app/config/config.neon');
if (isset($_SERVER["VPS"]) && $_SERVER["VPS"]) {
	error_reporting(E_ALL ^ E_DEPRECATED ^ E_USER_DEPRECATED);
	$configurator->addConfig(rootDir . '/app/config/config.vps.neon');
}
else if ($configurator->isDebugMode() || (isset($_SERVER["DEVELOPMENT"]) && isset($_SERVER["DEVELOPMENT"]) == TRUE) || (php_sapi_name() == "cli" && isset($_SERVER["USER"]) && $_SERVER["USER"] == "jam")) {
	error_reporting(E_ALL ^ E_DEPRECATED ^ E_USER_DEPRECATED);
	$configurator->addConfig(rootDir . '/app/config/config.local.neon');
}
else {
	$configurator->addConfig(rootDir . '/app/config/config.public.neon');
}
$container = $configurator->createContainer();

return $container;

