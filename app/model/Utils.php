<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 17. 11. 2014
 * Time: 2:06
 */

namespace App\Model;

use Nette;

class Utils
{
	/**
	 * @param      $cena
	 * @param bool $noFree
	 * @return string
	 */
	public static function cenaCreate($cena, $append = NULL, $free = TRUE) {
		if ($cena <= 0 && $free) return "zdarma";

		if (is_null($append)) $append = "Kč";

		$cena = ceil($cena);

		$delka = strlen($cena);
		$trojic = ceil($delka / 3);

		$tempCena = "";
		for ($i = 0; $i < $trojic - 1; $i++) {
			if ($tempCena != "") $tempCena = "." . $tempCena;
			$tempCena = self::numI($cena, $i * 3 + 2) . self::numI($cena, $i * 3 + 1) . self::numI($cena, $i * 3) . $tempCena;
		}

		$tempCena2 = ceil(self::numI($cena, ($trojic - 1) * 3 + 2) * 100 + self::numI($cena, ($trojic - 1) * 3 + 1) * 10 + self::numI($cena, ($trojic - 1) * 3));

		if ($tempCena != "" && $tempCena2 > 0) $tempCena = $tempCena2 . "." . $tempCena;
		else $tempCena = $tempCena2;

		return $tempCena . ",- " . $append;
	}
	/**
	 * @param $num
	 * @param $i
	 * @return int
	 */
	private static function numI($num, $i) {
		return (floor($num / pow(10, $i))) % 10;
	}
	/**
	 * @param $string
	 * @return bool
	 */
	public static function isJson($string) {
		json_decode($string);

		return (json_last_error() == JSON_ERROR_NONE);
	}

	/**
	 * @return array
	 */
	public static function getServerInfo() {
		$info = array(
			"HTTP_ORIGIN"           => isset($_SERVER["HTTP_ORIGIN"]) ? $_SERVER["HTTP_ORIGIN"] : "",
			"HTTP_USER_AGENT"       => isset($_SERVER["HTTP_USER_AGENT"]) ? $_SERVER["HTTP_USER_AGENT"] : "",
			"REDIRECT_QUERY_STRING" => isset($_SERVER["REDIRECT_QUERY_STRING"]) ? $_SERVER["REDIRECT_QUERY_STRING"] : "",
			"QUERY_STRING"          => isset($_SERVER["QUERY_STRING"]) ? $_SERVER["QUERY_STRING"] : "",
		);

		return $info;
	}
	public static function getTextServerInfo() {
		return print_r(self::getServerInfo(), TRUE);
	}
}
