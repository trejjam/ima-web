<?php
/**
 * Created by PhpStorm.
 * User: jam
 * Date: 10.2.15
 * Time: 13:05
 */

namespace App\Model;


use Nette,
	App,
	Trejjam,
	Symfony,
	Symfony\Component\Console\Input\InputInterface,
	Symfony\Component\Console\Output\OutputInterface;

class ChangelogTask extends Symfony\Component\Console\Command\Command
{
	/**
	 * @var Nette\Database\Context @inject
	 */
	private $database;
	/**
	 * @var string
	 */
	private $appDir;

	function __construct($appDir, Nette\Database\Context $database) {
		parent::__construct();

		$this->appDir = $appDir;
		$this->database = $database;
	}

	protected function configure() {
		$this->setName('Changelog:update')
			 ->setDescription('Install database schema (set-up DB credentials in config.local.neon).');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$output->writeln('Start changelog');

		$dir = $this->appDir . '/database/changelog.sql/';
		$sqlFiles = iterator_to_array(Nette\Utils\Finder::findFiles('*.sql')->from($dir)->getIterator());
		foreach ($sqlFiles as $k => $v) {
			unset($sqlFiles[$k]);
			$sqlFiles[str_replace($dir, '', $k)] = $v;
		}

		foreach ($this->database->table('about_changelog') as $v) {
			if (isset($sqlFiles[$v->filename])) {
				unset($sqlFiles[$v->filename]);
			}
		}

		usort($sqlFiles, [$this, 'changelogSort']);

		$connection = $this->database->getConnection();
		foreach ($sqlFiles as $v) {
			try {
				Nette\Database\Helpers::loadFromFile($connection, $v);
				$output->writeln('Changelog ' . $v . ' processed');
			}
			catch (\Exception $e) {
				$output->writeln('Error while processing ' . $v);

				throw $e;
			}
		}

		$output->writeln('Changelog done');
	}

	public function changelogSort($a, $b) {
		$aSubArr = explode('/', $a);
		$bSubArr = explode('/', $b);

		$aArr = explode('-', $aSubArr[count($aSubArr) - 1]);
		$bArr = explode('-', $bSubArr[count($bSubArr) - 1]);

		$aTextDate = $aArr[0] . '-' . $aArr[1] . '-' . $aArr[2];
		$bTextDate = $bArr[0] . '-' . $bArr[1] . '-' . $bArr[2];

		$aDate = new Nette\Utils\DateTime($aTextDate);
		$bDate = new Nette\Utils\DateTime($bTextDate);

		if ($aDate->getTimestamp() == $bDate->getTimestamp()) {
			return filemtime($a) < filemtime($b) ? '-1' : '1';
		}
		else {
			return $aDate->getTimestamp() < $bDate->getTimestamp() ? '-1' : '1';
		}
	}
}
