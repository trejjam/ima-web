<?php
/**
 * Created by PhpStorm.
 * User: jam
 * Date: 8.12.14
 * Time: 3:32
 */

namespace App\Model;

use Nette;

class TreminMenu
{
	const
		TABLE = "tremin__menu";

	/**
	 * @var Nette\Database\Context
	 */
	private $database;
	/**
	 * @var \Trejjam\Authorization\Acl
	 */
	private $acl;
	/**
	 * @var Nette\Security\User
	 */
	private $user;

	function __construct(Nette\Database\Context $database, \Trejjam\Authorization\Acl $acl, Nette\Security\User $user) {
		$this->database = $database;
		$this->acl = $acl;
		$this->user = $user;
	}

	function getMenu() {
		$out = [];

		$user = $this->user;
		foreach ($this->database->table(self::TABLE)->where(["enable" => "enable"]) as $v) {
			try {
				$resources = $this->acl->getResourceById($v->resource_id);

				list($resourceName, $resourceAction) = $resources->getAllowedList();
				if (!$user->isAllowed($resourceName, $resourceAction)) {
					continue;
				}
				$r = (object)[
					"url"  => $v->url_name,
					"name" => $v->name,
				];
			}
			catch (\Exception $e) {
				$r = (object)[
					"url"  => $v->url_name,
					"name" => $v->name,
				];
			}

			$out[] = $r;
		}

		return $out;
	}
}