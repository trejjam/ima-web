<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 16. 11. 2014
 * Time: 0:19
 */

namespace App\Model;

use Nette;

class PageInfo
{
	const TABLE     = "page_info";
	const ROOT_PAGE = "1";

	private $cacheParams = [
		Nette\Caching\Cache::EXPIRE => '60 minutes'
	];

	/**
	 * @var Nette\Caching\Cache
	 */
	private $cache;
	/**
	 * @var Nette\Database\Context
	 */
	private $database;
	/**
	 * @var LabelsService
	 */
	private $labels;

	/**
	 * @var Page[]
	 */
	private $pages = [];
	/**
	 * @var Page[]
	 */
	private $rootPages = [];

	private $generated = NULL;

	function __construct(Nette\Caching\Cache $cache, Nette\Database\Context $database, LabelsService $labels) {
		$this->cache = $cache;
		$this->database = $database;
		$this->labels = $labels;
	}

	/**
	 * @return Page[]
	 */
	public function getTree() {
		if (count($this->rootPages) == 0) {
			if (isset($this->cache["page_list"])) {
				foreach (unserialize($this->cache["page_list"]) as $k => $v) {
					if (isset($this->cache["page_" . $v])) {
						$this->rootPages[$v] = unserialize($this->cache["page_" . $v]);
						$this->rootPages[$v]->fillArray($this->pages);
					}
				}
			}
			else {
				$this->createTree();
			}
		}

		return $this->rootPages;
	}
	private function createTree() {
		foreach ($this->database->table(self::TABLE) as $v) {
			$this->pages[$v->id] = new Page($v);
		}

		foreach ($this->pages as $v) {
			$v->connectToParent($this->pages);

			if (!$v->hasParent()) {
				$this->rootPages[$v->getId()] = $v;

				if ($v->getId() != self::ROOT_PAGE) {
					$v->setPseudoParent($this->pages[self::ROOT_PAGE]);
				}
			}
		}


		$treeCacheParams = array_merge_recursive($this->cacheParams, [
			Nette\Caching\Cache::TAGS => [
				"pageTree"
			]
		]);

		$this->cache->save("page_list", serialize(array_keys($this->rootPages)), $treeCacheParams);
		foreach ($this->rootPages as $v) {
			$this->cache->save("page_" . $v->getId(), serialize($v), $treeCacheParams);
		}
	}

	/**
	 * @param Nette\Application\Request $request
	 * @return Page
	 */
	public function selectPage(Nette\Application\Request $request) {
		$tree = $this->getTree();
		$selected = NULL;
		foreach ($tree as $v) {
			if ($v->getPage() == $request->getPresenterName()) {
				$selected = $v;

				$selected = $selected->findCandidateByParams($request->getParameters());

				break;
			}
		}

		if (is_null($selected)) {
			$selected = $this->pages[self::ROOT_PAGE];
		}

		return $selected;
	}
	/**
	 * @param int $id
	 * @return Page|null
	 */
	public function getPage($id) {
		$this->getTree();
		foreach ($this->pages as $v) {
			if ($v->getId() == $id) {
				return $v;
			}
		}

		return NULL;
	}
	/**
	 * @param string $name
	 * @return Page|null
	 */
	public function getRootPage($name) {
		foreach ($this->pages as $v) {
			if ($v->getPage() == $name) {
				return $v;
			}
		}

		return NULL;
	}
	public function getHead(Page $page) {
		$out = new \stdClass();
		$out->id = $page->getId();
		$out->title = $page->getTitleText() . $this->labels->getData("pageTitle");
		$out->description = $page->getText("description");
		$out->keywords = $page->getText("keywords");
		$out->fbImg = $page->getText("img");

		$this->generated = $out;

		return $out;
	}
	public function getGenerated() {
		return $this->generated;
	}
}

class Page
{
	private $id;
	private $parentId;
	private $page;
	private $subAttribute;
	private $title;
	private $description;
	private $keywords;
	private $img;
	/**
	 * @var Page
	 */
	private $parent;
	/**
	 * @var Page
	 */
	private $pseudoParent = NULL; //link for root page to home page
	/**
	 * @var Page[]
	 */
	private $child = [];

	public function __construct(Nette\Database\Table\ActiveRow $row) {
		foreach ([
					 "id",
					 "parentId"     => "parent_id",
					 "page",
					 "subAttribute" => "sub_attribute",
					 "title",
					 "description",
					 "keywords",
					 "img",
				 ] as $k => $v) {
			if (is_numeric($k)) $k = $v;
			$this->$k = $row->$v;
		}
	}

	public function setPseudoParent(Page $parent) {
		$this->pseudoParent = $parent;
	}
	/**
	 * @param array $pages
	 */
	public function connectToParent(array $pages) {
		if (!$this->hasParent()) return;
		$this->parent = $pages[$this->parentId];
		$this->parent->connectToChild($this);
	}
	/**
	 * @param Page $page
	 */
	public function connectToChild(Page $page) {
		$this->child[$page->getId()] = $page;
	}
	/**
	 * @param array $pages
	 */
	public function fillArray(array &$pages) {
		$pages[$this->getId()] = $this;
		foreach ($this->child as $v) {
			$v->fillArray($pages);
		}
	}
	/**
	 * @return bool
	 */
	public function hasParent() {
		return !is_null($this->parentId);
	}
	public function hasPseudoParent() {
		return !is_null($this->pseudoParent);
	}

	public function findCandidateByParams(array $params) {
		if (!is_null($this->subAttribute) && isset($params[$this->subAttribute])) {
			foreach ($this->child as $v) {
				if ($v->getPage() == $params[$this->subAttribute]) {
					return $v->findCandidateByParams($params);
				}
			}
		}

		return $this;
	}

	public function getId() {
		return $this->id;
	}
	/**
	 * @return Page
	 */
	public function getParent() {
		if ($this->hasParent()) {
			return $this->parent;
		}

		return NULL;
	}
	public function getPseudoParent() {
		if ($this->hasPseudoParent()) {
			return $this->pseudoParent;
		}

		return NULL;
	}
	/**
	 * @return Page|null
	 */
	public function findParent($usePseudo = TRUE) {
		if ($this->hasParent()) {
			return $this->getParent();
		}
		else if ($usePseudo && $this->hasPseudoParent()) {
			return $this->getPseudoParent();
		}

		return NULL;
	}
	public function getPage() {
		return $this->page;
	}
	public function getTitleText() {
		$out = "";
		foreach ($this->getRootWay() as $v) {
			if (!is_null($v->getTitle())) {
				$out = $v->getTitle() . " - " . $out;
			}
			if ($out == "" && $v->hasPseudoParent() && !is_null($v->getPseudoParent()->getTitle())) {
				$out = $v->getPseudoParent()->getTitle() . " - ";
			}
		}

		return $out;
	}
	public function getText($name) {
		$get = "get" . Nette\Utils\Strings::firstUpper($name);
		foreach ($this->getRootWay() as $v) {
			if (!is_null($v->$get())) {
				return $v->$get();
			}
			if ($v->hasPseudoParent() && !is_null($v->getPseudoParent()->$get())) {
				return $v->getPseudoParent()->$get();
			}
		}

		return "";
	}

	public function getTitle() {
		return $this->title;
	}
	public function getDescription() {
		return $this->description;
	}
	public function getKeywords() {
		return $this->keywords;
	}
	public function getImg() {
		return $this->img;
	}

	/**
	 * @return Page[]
	 */
	public function getChild() {
		return $this->child;
	}
	/**
	 * @return Page[]
	 */
	public function getRootWay() {
		if (!$this->hasParent()) {
			return [$this];
		}
		else {
			$parentWay = $this->parent->getRootWay();
			$parentWay[] = $this;

			return $parentWay;
		}
	}
}
