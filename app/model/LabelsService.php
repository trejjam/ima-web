<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 3.5.14
 * Time: 19:06
 */

namespace App\Model;

use Nette;

class LabelsService extends Nette\Object
{
	/**
	 * @var Nette\Database\Context
	 */
	private $database;
	/**
	 * @var \App\Components\Label
	 */
	private $label;
	private $labels = array();

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
		$this->label = new \App\Components\Label();
		$this->label->setService($this);
	}
	public function create() {
		return $this->label;
	}
	public function getData($name) {
		if (isset($this->labels[$name]) && $this->labels[$name] != "") {
			return $this->labels[$name];
		}
		else {
			//$value=$this->database->table("labels")->select("value")->where(array("name"=>$name))->fetch();
			foreach ($this->database->table("labels") as $v) {
				$this->labels[$v->name] = $v->value;
			}
			//if (is_object($value)) {
			if (isset($this->labels[$name])) {
				//return $this->labels[$name]=$value->value;
				return $this->labels[$name];
			}
			else {
				return $this->labels[$name] = "";
			}
		}
	}
	public function setData($name, $data) {
		$this->database->table("labels")->where(array("name" => $name))->update(array("value" => $data));
		$this->labels[$name] = $data;
	}
	function &__get($name) {
		$this->getData($name);

		return $this->labels[$name];
	}
	function __set($name, $data) {
		$this->setData($name, $data);

		return $data;
	}
}
