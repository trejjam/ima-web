<?php

namespace App\Presenters;

use Nette,
	Nette\Application\UI;


/**
 * Map presenter.
 */
class MapPresenter extends BasePresenter
{
	/**
	 * @var int @persistent
	 */
	public $tagId = 0;

	const
		MAP_TABLE = "maps",
		MAP_FILE = "file",
		MAP_FOLDER = "maps";

	const
		ROOM_TABLE = "maps__room",
		ROOM_MAP = "map_id",
		ROOM_NAME = "name";

	const
		RECT_TABLE = "maps__room_rectangles",
		RECT_ROOM = "room_id",
		RECT_X1 = "x1",
		RECT_X2 = "x2",
		RECT_Y1 = "y1",
		RECT_Y2 = "y2";

	const
		TAG_TABLE = "log__tag",
		TAG_NUMBER = "tag_number";

	const
		TAG_LEARN_HISTORY_TABLE = "maps__tag_learn_history",
		TAG_LEARN_HISTORY_TAG = "tag_id",
		TAG_LEARN_HISTORY_ROOM = "room_id",
		TAG_LEARN_HISTORY_USER = "user_id";

	/**
	 * @var Nette\Database\Context @inject
	 */
	public $database;

	public function handleTagInRoom($roomId) {
		if ($this->tagId == 0) {
			$this->flashMessage("Please select your tag.", json_encode(["type" => "fail", "width" => "300"]));

			return;
		}

		$this->database->table(self::TAG_LEARN_HISTORY_TABLE)->insert([
			self::TAG_LEARN_HISTORY_TAG  => $this->tagId,
			self::TAG_LEARN_HISTORY_ROOM => $roomId,
			self::TAG_LEARN_HISTORY_USER => $this->getUser()->id,
		]);
	}

	public function renderDashboard() {
		$maps = [];

		foreach ($this->database->table(self::MAP_TABLE) as $v) {
			$maps[$v->id] = $v->{self::MAP_FILE};
		}

		$this->template->editEnable = $this->getUser()->isAllowed("map", "edit");
		$this->template->maps = $maps;
	}
	public function renderEdit($id) {
		$template = $this->template;

		$map = $this->database->table(self::MAP_TABLE)->get($id);

		$template->mapId = $id;
		$template->map = $map->{self::MAP_FILE};
		$template->mapUrl = self::DATA_DIR . "/" . self::MAP_FOLDER . "/" . $map->{self::MAP_FILE};

		$rooms = [];
		foreach ($map->related(self::ROOM_TABLE, self::ROOM_MAP) as $v) {
			if (!isset($rooms[$v->id])) $rooms[$v->id] = [];

			foreach ($v->related(self::RECT_TABLE, self::RECT_ROOM) as $v2) {
				$rooms[$v->id][] = (object)[
					"id" => $v2->id,
					"x1" => $v2->{self::RECT_X1},
					"x2" => $v2->{self::RECT_X2},
					"y1" => $v2->{self::RECT_Y1},
					"y2" => $v2->{self::RECT_Y2},
				];
			}
		}
		$template->rooms = $rooms;
	}
	public function renderLearn($id) {
		$template = $this->template;

		$map = $this->database->table(self::MAP_TABLE)->get($id);

		$template->mapId = $id;
		$template->map = $map->{self::MAP_FILE};
		$template->mapUrl = self::DATA_DIR . "/" . self::MAP_FOLDER . "/" . $map->{self::MAP_FILE};

		$mapUrlArr = explode(".", $map->{self::MAP_FILE});
		$mapUrlArr[count($mapUrlArr) - 2] .= "_small";
		$mapUrlSmall = implode(".", $mapUrlArr);
		if (is_file(rootDir . self::PUBLIC_DATA_DIR . "/" . self::MAP_FOLDER . "/" . $mapUrlSmall)) {
			$template->mapUrl = self::DATA_DIR . "/" . self::MAP_FOLDER . "/" . $mapUrlSmall;
		}

		$rooms = [];
		foreach ($map->related(self::ROOM_TABLE, self::ROOM_MAP) as $v) {
			if (!isset($rooms[$v->id])) $rooms[$v->id] = [];

			foreach ($v->related(self::RECT_TABLE, self::RECT_ROOM) as $v2) {
				$rooms[$v->id][] = (object)[
					"id"   => $v2->id,
					"x1"   => $v2->{self::RECT_X1},
					"x2"   => $v2->{self::RECT_X2},
					"y1"   => $v2->{self::RECT_Y1},
					"y2"   => $v2->{self::RECT_Y2},
					"name" => $v->{self::ROOM_NAME}
				];
			}
		}
		$template->rooms = $rooms;
	}

	function createComponentUploadMap() {
		$form = new UI\Form();

		$form->addUpload("map")
			 ->addRule($form::IMAGE, 'The file must be JPEG, PNG or GIF.');;

		$form->addSubmit('send', 'Upload');
		$form->onSuccess[] = $this->uploadMapFormSucceeded;

		return $form;
	}
	public function uploadMapFormSucceeded(UI\Form $form) {
		$values = $form->getValues();

		if (!$this->getUser()->isAllowed("map", "edit")) return;

		if ($values->map->isOk()) {
			$filename = $values->map->getSanitizedName();
			$targetPath = rootDir . self::PUBLIC_DATA_DIR . "/" . self::MAP_FOLDER;
			@mkdir($targetPath, 0660, TRUE);

			$map = $this->database->table(self::MAP_TABLE)->insert([]);

			$values->map->move("$targetPath/" . $map->id . "_$filename");

			$map->update([
				self::MAP_FILE => $map->id . "_$filename"
			]);

			$this->flashMessage("Map has been successfully uploaded.", json_encode(["type" => "success", "width" => "300"]));

			$this->redirect("this");
		}
		else {
			$this->flashMessage("When uploading error occurred.", json_encode(["type" => "fail", "width" => "300"]));
		}
	}

	function createComponentAddRoom() {
		return new UI\Multiplier(function ($id) {
			$form = new UI\Form();

			$form->addText("room")
				 ->setAttribute('placeholder', 'Room')
				 ->setRequired('Please enter folder name.');

			$form->addHidden("map", $id);

			$form->addSubmit('send', 'Add');
			$form->onSuccess[] = $this->addRoomFormSucceeded;

			return $form;
		});
	}
	public function addRoomFormSucceeded(UI\Form $form) {
		$values = $form->getValues();

		if (!$this->getUser()->isAllowed("map", "edit")) return;
		if (!Nette\Utils\Validators::isNumericInt($values->map)) return;

		$dbArr = [
			self::ROOM_NAME => $values->room,
			self::ROOM_MAP  => $values->map,
		];

		if ($this->database->table(self::ROOM_TABLE)->where($dbArr)->fetch()) {
			$this->flashMessage("Room already exist.", json_encode(["type" => "fail", "width" => "300"]));

			return;
		}

		$this->database->table(self::ROOM_TABLE)->insert($dbArr);

		$this->flashMessage("Room has been successfully added.", json_encode(["type" => "success", "width" => "300"]));

		$this->redirect("this");
	}
	function createComponentShowRoom() {
		return new UI\Multiplier(function ($id) {
			$form = new UI\Form();

			$map = $this->database->table(self::MAP_TABLE)->get($id);
			$room = [0 => "all"];
			foreach ($map->related(self::ROOM_TABLE, self::ROOM_MAP) as $v) {
				$room[$v->id] = $v->{self::ROOM_NAME};
			}

			$form->addSelect("room", NULL, $room);

			return $form;
		});
	}

	function createComponentSelectRoom() {
		return new UI\Multiplier(function ($id) {
			$form = new UI\Form();

			$map = $this->database->table(self::MAP_TABLE)->get($id);
			$room = [];
			foreach ($map->related(self::ROOM_TABLE, self::ROOM_MAP) as $v) {
				$room[$v->id] = $v->{self::ROOM_NAME};
			}

			$form->addSelect("room", NULL, $room);

			$form->addHidden("x1");
			$form->addHidden("x2");
			$form->addHidden("y1");
			$form->addHidden("y2");

			$form->addSubmit('send', 'Append');
			$form->onSuccess[] = $this->selectRoomFormSucceeded;

			return $form;
		});
	}
	public function selectRoomFormSucceeded(UI\Form $form) {
		$values = $form->getValues();

		if (!$this->getUser()->isAllowed("map", "edit")) return;
		if (!Nette\Utils\Validators::isNumericInt($values->room)) return;

		$dbArr = [
			self::RECT_ROOM => $values->room,
			self::RECT_X1   => $values->x1,
			self::RECT_X2   => $values->x2,
			self::RECT_Y1   => $values->y1,
			self::RECT_Y2   => $values->y2,
		];

		$this->database->table(self::RECT_TABLE)->insert($dbArr);

		$this->flashMessage("Room has been successfully modified.", json_encode(["type" => "success", "width" => "300"]));

		$this->redirect("this");
	}

	function createComponentSelectTag() {
		$form = new UI\Form();

		$tags = ["without tag"];
		foreach ($this->database->table(self::TAG_TABLE) as $v) {
			$tags[$v->id] = $v->{self::TAG_NUMBER} . " - 0x" . dechex($v->{self::TAG_NUMBER});
		}

		$tag = $form->addSelect("tag", NULL, $tags);

		$tag->setDefaultValue($this->tagId);

		$form->addSubmit('send', 'Select');
		$form->onSuccess[] = $this->selectTagFormSucceeded;

		return $form;
	}
	function selectTagFormSucceeded(UI\Form $form) {
		$values = $form->getValues();

		if (!$this->getUser()->isAllowed("map", "learn")) return;
		if (!Nette\Utils\Validators::isNumericInt($values->tag)) return;

		if ($values->tag == 0 && $this->tagId != 0) {
			$this->database->table(self::TAG_LEARN_HISTORY_TABLE)->insert([
				self::TAG_LEARN_HISTORY_TAG  => $this->tagId,
				self::TAG_LEARN_HISTORY_USER => $this->getUser()->id,
			]);

			$this->redirect("this", ["tagId" => NULL]);
		}

		$this->redirect("this", ["tagId" => $values->tag]);
	}
}
