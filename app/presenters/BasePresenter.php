<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	use \App\Layout\BaseLayoutTrait;

	const
		DATA_DIR = "/data",
		PUBLIC_DATA_DIR = "/www/data",
		BIG_DATA_DIR = "/bigData";

	/**
	 * @var \Trejjam\Authorization\Acl @inject
	 */
	public $acl;
	/**
	 * @var \App\Model\TreminMenu @inject
	 */
	public $treminMenu;

	public function ownBeforeRender() {
		if (!$this->getUser()
				  ->isLoggedIn() && $this->name != "Sign" && ($this->action != "in" || $this->action != "default")
		) {
			$this->redirect("Sign:in");
		}

		if ($this->getUser()->isLoggedIn()) {
			$this->template->menu = $this->treminMenu->getMenu();
		}
	}
}
