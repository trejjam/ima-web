module.exports = (grunt) ->
	grunt.initConfig
		typescript:
			ts:
				src: ['www/js/ts/*.ts', '!**/*.d.ts']
				dest: 'www/js/ts'

				options:
					module: 'amd'
					basePath: 'www/js/ts',
					sourceMap: true,
					declaration: true

		uglify:
			js:
				files:
					'www/js/main-all.min.js': [
						'www/js/jquery-1.11.1.min.js'
						'www/js/netteForms.js'
						'www/js/nette.ajax.js'
						'www/js/ts/commonFunc.js'
						'www/js/ts/popups.js'
						'www/js/ts/select.js'
						'www/js/ts/IMAMap.js'
						'www/js/main.js'
					]
		lessTask:
			less:
				options:
					ieCompat: true
					sourceMap: true
					sourceMapFilename: 'www/css/root.css.map'
					sourceMapBasepath: 'www/css'
					sourceMapURL: 'root.css.map'
				files: [
					expand: true
					cwd: 'www/less',
					src: ['*.less', '!*.f.less']
					dest: 'www/css'
					ext: '.css'
				]

		cssmin:
			css:
				files: [
					expand: true
					cwd: 'www/css/'
					src: [
						'*.css'
						'!*.min.css'
					]
					dest: 'www/css'
					ext: '.min.css'
				]

		watchTask:
			watchLess:
				files: ['www/less/*.less']
				tasks: ['less']
				options:
					livereload: true
					nospaces: true
					interrupt: true
					debounceDelay: 200
			watchJs:
				files: ['www/js/*.js', '!**/*.min.js']
				tasks: ['js']
				options:
					livereload: 35730
					nospaces: true
					interrupt: true
					debounceDelay: 200
			watchTs:
				files: ['www/js/*.js', '!www/js/main-all.min.js', '!www/js/all-libs.min.js', 'www/js/ts/*.ts',
						'!**/*.d.ts']
				tasks: ['ts']
				options:
					livereload: 35730
					nospaces: true
					interrupt: true
					debounceDelay: 200

		concurrent:
			multiWatch:
				tasks: ['watchTask:watchLess', 'watchTask:watchTs'],
				options:
					logConcurrentOutput: true


	# These plugins provide necessary tasks.
	grunt.loadNpmTasks 'grunt-contrib-uglify'
	grunt.loadNpmTasks 'grunt-contrib-cssmin'
	grunt.loadNpmTasks 'grunt-contrib-less'
	grunt.loadNpmTasks 'grunt-typescript'
	grunt.renameTask 'less', 'lessTask'
	grunt.loadNpmTasks 'grunt-concurrent'
	grunt.loadNpmTasks 'grunt-contrib-watch'
	grunt.renameTask 'watch', 'watchTask'

	# Default task.
	grunt.registerTask 'ts', [
		'typescript', 'js'
	]

	grunt.registerTask 'js', [
		'uglify'
	]

	grunt.registerTask 'less', [
		'lessTask:less', 'css'
	]

	grunt.registerTask 'css', [
		'cssmin'
	]

	grunt.registerTask 'watch', [
		'concurrent:multiWatch'
	]

	grunt.registerTask 'default', [
		'js', 'less', 'css'
	]

	grunt.registerTask 'deploy', [
		'ts', 'less'
	]
